﻿using SAR.Models;
using SAR.Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAR.Support
{
    public class Graphic
    {
        private OffenseContext db = new OffenseContext();

        public List<DataPoint> Paint()
        {
            List<Offense> offenses = db.Offenses.ToList();

            //данные для графика
            List<DataPoint> dataPoints = new List<DataPoint> { };

            var brigades = from brigade in offenses
                           group brigade by brigade.Brigade into g
                           select new { Name = g.Key, Count = g.Count() };
            brigades = brigades.OrderBy(m => m.Name);

            foreach (var item in brigades)
                dataPoints.Add(new DataPoint("Бригада, №" + item.Name, item.Count));

            return dataPoints;
        }

        public List<DataPoint> PaintHeading()
        {
            List<Offense> offenses = db.Offenses.ToList();

            //данные для графика
            List<DataPoint> dataPoints = new List<DataPoint> { };

            var headings = from heading in offenses
                           group heading by heading.Heading into g
                           select new { Name = g.Key, Count = g.Count() };
            

            foreach (var item in headings)
                dataPoints.Add(new DataPoint( item.Name, item.Count));

            return dataPoints;
        }

        public List<DataPoint> PaintObject()
        {
            List<Offense> offenses = db.Offenses.ToList();

            //данные для графика
            List<DataPoint> dataPoints = new List<DataPoint> { };

            var objects = from o in offenses
                          group o by o.Object into g
                           select new { Name = g.Key, Count = g.Count() };
            

            foreach (var item in objects)
                dataPoints.Add(new DataPoint("Объект: " + item.Name.ToString(), item.Count));

            return dataPoints;
        }

        public List<DataPoint> PaintOpenOffenses()
        {
            List<Offense> offenses = db.Offenses.ToList();

            //данные для графика
            List<DataPoint> dataPoints = new List<DataPoint> { };

            var objects = from o in offenses
                          where o.Status == true
                          group o by o.Brigade into g
                          select new { Name = g.Key, Count = g.Count() };
            objects = objects.OrderBy(m => m.Name);

            foreach (var item in objects)
                dataPoints.Add(new DataPoint("Бригада, №" + item.Name.ToString(), item.Count));

            return dataPoints;
        }

        public List<DataPoint> PaintExpiredOffenses()
        {
            List<Offense> offenses = db.Offenses.ToList();

            //данные для графика
            List<DataPoint> dataPoints = new List<DataPoint> { };

            var objects = from o in offenses
                          where o.ActualDate > o.RequiredDate
                          group o by o.Brigade into g
                          select new { Name = g.Key, Count = g.Count() };
            objects = objects.OrderBy(m => m.Name);

            foreach (var item in objects)
                dataPoints.Add(new DataPoint("Бригада, №" + item.Name.ToString(), item.Count));

            return dataPoints;
        }
    }
}