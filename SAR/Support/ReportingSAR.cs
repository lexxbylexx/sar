﻿using SAR.Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SAR.Support
{
    public class ReportingSAR
    {
        private OffenseContext db = new OffenseContext();

        public string GetBodyMail()
        {
            StringBuilder body = new StringBuilder();

            var data = db.Offenses.Where(m => m.Status == false).OrderBy(m => m.Brigade);

            body.AppendLine("<h4>Здравствуйте!</h4><br>");
            body.AppendLine("<b>Вас приветствует автоматизированная система контроля предписаний.</b><br>");

            body.Append("На сегодняшний день предписаний всего: <b>");
            body.AppendLine(db.Offenses.Count().ToString());

            body.Append("</b><br>Не закрыто предписаний: <strong style =\"color: red\">");
            body.Append(db.Offenses.Where(m => m.Status == false).Count().ToString()+ "</strong>");
            body.Append("<br><br>");
            body.Append("<i>Информация по незакрытым предписаниям:</i><br>");
            if (data != null)
            {
                body.Append("<table border=\"1\">");
                body.Append("<tr align=\"center\"  style=\"background - color:silver \"><td><strong>Бригада,№</strong></td><td><strong>Дата выдачи</strong></td><td><strong>Описание</strong></td><td><strong>Ответственное лицо</strong></td><td><strong>Дней просрочено/осталось</strong></td></tr>");
                foreach (var item in data)
                {
                    int a = (item.RequiredDate).Subtract(DateTime.Now).Days;
                    string prosr = (a >= 0) ? ("<strong style=\"color: green\">" + a +"</strong>") : ("<strong style=\"color: red\">" + (-a) +"</strong>");

                    StringBuilder s = new StringBuilder("<tr><td align=\"right\">" + item.Brigade + "</td><td align=\"center\">" + item.Date.ToShortDateString()+ "</td><td>" + item.Prescription + "</td><td>" + item.ResponsiblePerson + "</td><td align=\"right\">" + prosr+"</td></tr>");
                    body.Append(s);
                }
                body.Append("<table>");
            }
            else
            {
                body.Clear();
                body.Append("Нет доступа к базе данных");
            }

            body.AppendLine("<br align=\"right\">С уважением,<br align=\"right\">Отдел ОТ, ПБ и ООС ООО \"ВПТ-НЕФТЕМАШ\"");
            body.AppendLine("<hr>");
            db.Dispose();
            return body.ToString();
        }

        
    }
}