﻿using SAR.Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web;

namespace SAR.Support
{
    public class MailSenderSAR : IHttpModule
    {
        public string Adresses { get; set; }
        public string BodyMail { get; set; }

        private static Timer timer;
        private long interval = 30000; //30 секунд
        private static object synclock = new object();
        private static bool sent = false;
        private string adresses= "valeriy.leonov@inbox.ru";
        private string bodyMail = "";

        private int hour = 21;
        private int minute = 24;

        private OffenseContext db = new OffenseContext();
        private ReportingSAR r = new ReportingSAR();

        public MailSenderSAR()
        {
            //передаем адресаты для отчета

            var settings = db.Settings.ToList();

            if (settings != null)
            foreach (var item in settings)
            {
                adresses += ("," + item.Adresses);
            }

            //передаем текст для отчета

            bodyMail = r.GetBodyMail();
        }

        public MailSenderSAR(string a, string b)
        {
            adresses = a;
            bodyMail = b;
        }

        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            timer = new Timer(new TimerCallback(SendMail), null, 0, interval);
        }

        private void SendMail(object obj)
        {
            lock (synclock)
            {
                DateTime dd = DateTime.Now;
                if (dd.Hour == hour && dd.Minute == minute && sent == false)
                {
                    MailMessage mail = new MailMessage();
                    mail.IsBodyHtml = true;
                    mail.From = new MailAddress("vleonov@vptn.ru"); // Адрес отправителя
                    mail.To.Add(adresses); // Адрес получателя

                    mail.Subject = "Отчет по предписаниям на " + DateTime.Now.ToShortDateString();
                    mail.Body = bodyMail;

                    SmtpClient client = new SmtpClient();
                    client.Host = "smtp.yandex.ru";
                    client.Port = 587; // Обратите внимание что порт 587
                    client.EnableSsl = true;
                    client.Credentials = new NetworkCredential("vleonov@vptn.ru", "vptn1988"); // Ваши логин и пароль
                    client.Send(mail);
                    mail.Dispose();
                    sent = true;
                }
                else if (dd.Hour != hour && dd.Minute != minute)
                {
                    sent = false;
                }
            }

            db.Dispose();
        }
    }
}