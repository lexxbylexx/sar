﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SAR.Models
{
    public class AppDbInitializer : DropCreateDatabaseAlways<ApplicationContext>
    {
        protected override void Seed(ApplicationContext context)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // создаем две роли
            var role1 = new IdentityRole { Name = "admin" };
            var role2 = new IdentityRole { Name = "hse" };
            var role3 = new IdentityRole { Name = "employyer" };
            // добавляем роли в бд
            roleManager.Create(role1);
            roleManager.Create(role2);
            roleManager.Create(role3);

            // создаем пользователей
            var admin = new ApplicationUser { Email = "vleonov@vptn.ru", UserName = "vleonov@vptn.ru" };
            string password = "vptn1988";
            var result = userManager.Create(admin, password);

            var employyer = new ApplicationUser { Email = "mlitvina@vptn.ru", UserName = "mlitvina@vptn.ru" };
            password = "vptn1988";
            result = userManager.Create(employyer, password);

            var hse = new ApplicationUser { Email = "izinoviev@vptn.ru", UserName = "izinoviev@vptn.ru" };
            password = "vptn1988";
            result = userManager.Create(hse, password);
            // если создание пользователя прошло успешно
            if (result.Succeeded)
            {
                // добавляем для пользователя роль
                userManager.AddToRole(admin.Id, role1.Name);
                userManager.AddToRole(employyer.Id, role3.Name);
                userManager.AddToRole(hse.Id, role2.Name);
            }

            base.Seed(context);
        }
    }
}