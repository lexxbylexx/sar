﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SAR.Models.EF
{
    public class OffenseContext : DbContext

    {
        public DbSet<Offense> Offenses { get; set; }

        public DbSet<Setting> Settings { get; set; }
        public object Users { get; internal set; }
    }
}