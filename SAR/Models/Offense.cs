﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SAR.Models
{
    public class Offense
    {
        public int Id { get; set; }

        [Display(Name = "Объект")]
        public string Object { get; set; }

        [Display(Name = "Лицо, выдавшее предписание")]
        public string PersonIssued { get; set; }

        [Display(Name = "Бригада,№")]
        public int Brigade { get; set; }

        [Display(Name = "Номер предписания")]
        public int Number { get; set; }

        [Display(Name = "Дата выдачи")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Display(Name = "Область нарушения")]
        public string Heading { get; set; }

        [Display(Name = "Текст предписания")]
        public string Prescription { get; set; }

        [Display(Name = "Ссылка на номативную базу")]
        public string Link { get; set; }

        [Display(Name = "Корректирующее мероприятие")]
        public string Action { get; set; }

        [Display(Name = "Отвественное лицо")]
        public string ResponsiblePerson { get; set; }

        [Display(Name = "Назначенная дата устранения")]
        [DataType(DataType.Date)]
        public DateTime RequiredDate {get; set;}

        [Display(Name = "Фактическая дата устранения")]
        [DataType(DataType.Date)]
        public DateTime ActualDate {get; set;}

        [Display(Name = "Статус выполнения")]
        public bool Status { get; set; }

        [Display(Name = "Комментарий")]
        public string Comment { get; set; }

    }
}