﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SAR.Models;
using SAR.Models.EF;
using Newtonsoft.Json;
using PagedList;
using SAR.Support;

namespace SAR.Controllers
{
    public class OffensesController : Controller
    {
        private OffenseContext db = new OffenseContext();

        // GET: Offenses
        [Authorize]
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.BrigadeSortParm = String.IsNullOrEmpty(sortOrder) ? "brigade_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.NumberSortParm = sortOrder == "Number" ? "number_desc" : "Number";
            ViewBag.PersonSortParm = sortOrder == "Person" ? "person_desc" : "Person";
            ViewBag.ReqSortParm = sortOrder == "Req" ? "req_desc" : "Req";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var offenses = from s in db.Offenses
                           select s;

            //Поиск по мастеру или текмту предписания
            if (!String.IsNullOrEmpty(searchString))
            {

                offenses = offenses.Where(s => s.Prescription.Contains(searchString)
                                       || s.ResponsiblePerson.Contains(searchString));
            }

            //Сортировка
            switch (sortOrder)
            {
                case "brigade_desc":
                    offenses = offenses.OrderByDescending(s => s.Brigade);
                    break;
                case "Date":
                    offenses = offenses.OrderBy(s => s.Date);
                    break;
                case "date_desc":
                    offenses = offenses.OrderByDescending(s => s.Date);
                    break;
                case "Number":
                    offenses = offenses.OrderBy(s => s.Number);
                    break;
                case "number_desc":
                    offenses = offenses.OrderByDescending(s => s.Number);
                    break;
                case "Person":
                    offenses = offenses.OrderBy(s => s.ResponsiblePerson);
                    break;
                case "person_desc":
                    offenses = offenses.OrderByDescending(s => s.ResponsiblePerson);
                    break;
                case "Req":
                    offenses = offenses.OrderBy(s => s.RequiredDate);
                    break;
                case "req_desc":
                    offenses = offenses.OrderByDescending(s => s.RequiredDate);
                    break;
                default:
                    offenses = offenses.OrderBy(s => s.Brigade);
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(offenses.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public ActionResult OffenseSearch(string name)
        {
            var alloffenses = db.Offenses.Where(a => a.ResponsiblePerson.Contains(name)).ToList();
            return PartialView(alloffenses);
        }
        // GET: Offenses
        [Authorize]
        public async Task<ActionResult> Stat()
        {
            Graphic graphic = new Graphic();
            
            ViewBag.DataPoints = JsonConvert.SerializeObject(graphic.Paint());
            ViewBag.DataPointsObject = JsonConvert.SerializeObject(graphic.PaintObject());
            ViewBag.DataPointsHeading = JsonConvert.SerializeObject(graphic.PaintHeading());
            ViewBag.DataPointsOpen = JsonConvert.SerializeObject(graphic.PaintOpenOffenses());
            ViewBag.DataPointsExpired = JsonConvert.SerializeObject(graphic.PaintExpiredOffenses());

            return View(await db.Offenses.ToListAsync());
        }

        // GET: Offenses/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Offense offense = await db.Offenses.FindAsync(id);
            if (offense == null)
            {
                return HttpNotFound();
            }
            return View(offense);
        }

        // GET: Offenses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Offenses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Object,PersonIssued,Brigade,Number,Date,Heading,Prescription,Link,Action,ResponsiblePerson,RequiredDate,ActualDate,Status,Comment")] Offense offense)
        {
            if (ModelState.IsValid)
            {
                db.Offenses.Add(offense);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(offense);
        }

        // GET: Offenses/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Offense offense = await db.Offenses.FindAsync(id);
            if (offense == null)
            {
                return HttpNotFound();
            }
            return View(offense);
        }

        // POST: Offenses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Object,PersonIssued,Brigade,Number,Date,Heading,Prescription,Link,Action,ResponsiblePerson,RequiredDate,ActualDate,Status,Comment")] Offense offense)
        {
            if (ModelState.IsValid)
            {
                db.Entry(offense).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(offense);
        }

        // GET: Offenses/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Offense offense = await db.Offenses.FindAsync(id);
            if (offense == null)
            {
                return HttpNotFound();
            }
            return View(offense);
        }

        // POST: Offenses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Offense offense = await db.Offenses.FindAsync(id);
            db.Offenses.Remove(offense);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

     
    }
}
