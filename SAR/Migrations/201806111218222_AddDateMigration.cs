namespace SAR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDateMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Offenses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Object = c.String(),
                        PersonIssued = c.String(),
                        Brigade = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                        Date = c.String(),
                        Heading = c.String(),
                        Prescription = c.String(),
                        Link = c.String(),
                        Action = c.String(),
                        ResponsiblePerson = c.String(),
                        RequiredDate = c.String(),
                        ActualDate = c.String(),
                        Status = c.Boolean(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Offenses");
        }
    }
}
