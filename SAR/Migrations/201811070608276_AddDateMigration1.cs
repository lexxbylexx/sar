namespace SAR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDateMigration1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Offenses", "ResponsiblePerson", c => c.String());
            AlterColumn("dbo.Offenses", "ActualDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Offenses", "ActualDate", c => c.String());
            AlterColumn("dbo.Offenses", "ResponsiblePerson", c => c.DateTime(nullable: false));
        }
    }
}
